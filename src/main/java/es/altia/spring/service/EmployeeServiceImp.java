package es.altia.spring.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.altia.spring.model.Employee;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImp implements EmployeeService{
	
	private static HashMap<Integer,Employee> empleados = new HashMap<Integer,Employee>();
	
	public int save(Employee employee) {
		empleados.put(employee.getEmpno(),employee);
		return employee.getEmpno();
	}

	public Employee get(int id) {

		return empleados.get(id);
	}

	public List<Employee> list() {
		if (empleados.isEmpty()) {
			ArrayList<Employee> lista = new ArrayList<Employee>();
			lista.add(new Employee().setEname("Example"));
			return lista;
		}
		return new ArrayList<Employee>(empleados.values());
	}

	public void update(int id, Employee employee) {
		if (empleados.containsKey(id)) {
			if (id!=employee.getEmpno()) {
				empleados.remove(id);
				empleados.put(id, employee);
			}
			else empleados.replace(id,employee);
		}
	}

	public void delete(int id) {
		if (empleados.containsKey(id)) empleados.remove(id);
	}

	
}
