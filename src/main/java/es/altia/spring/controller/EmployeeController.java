package es.altia.spring.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import es.altia.spring.model.Employee;
import es.altia.spring.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired	
	private EmployeeService employeeService;
	
	//Alta de empleados
	@PostMapping("/employees")
	public ResponseEntity<?> save(@RequestBody Employee employee){
		int id = employeeService.save(employee);
		if (id>0) return (ResponseEntity<?>) ResponseEntity.ok().body("Se ha creado el usuario con codigo: "+id);
		return (ResponseEntity<?>) ResponseEntity.noContent();
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> get(@PathVariable("id") int id) {
		Employee employee = employeeService.get(id);
		return ResponseEntity.ok().body(employee);
	}
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> list() {
		List<Employee> empleados = employeeService.list();
		return ResponseEntity.ok().body(empleados);
	}
	@PutMapping("/employees/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Employee employee) {
		employeeService.update(id, employee);
		return ResponseEntity.ok().body("Employee has been updated successfully.");
	}
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<?> delete (@PathVariable("id") int id) {
		employeeService.delete(id);
		return ResponseEntity.ok().body("Employee has been deleted successfully.");
	}
}
